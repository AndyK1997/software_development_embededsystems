//
// Created by andyk on 24.04.2021.
//

#include "SensorTag.h"

SensorTag::SensorTag() {

}

int SensorTag::initRead(){
    return initializeSensortag();
}

int SensorTag::disconnect(){
    return disconnectBLEDevice();
}

int SensorTag::writeMovementConfig(){
    return SensorCommunication::writeConfig(*this);
}

Motion SensorTag::getMotion() {
    return SensorCommunication::getMotion(*this);
}

