//
// Created by andyk on 24.04.2021.
//

#ifndef PRAKTIKUM_1_MOTION_H
#define PRAKTIKUM_1_MOTION_H
#include "Accelerometer.h"
#include "Gyroscope.h"

class Motion {
    Accelerometer accelerometer;
    Gyroscope gyroscope;
public:
    const Accelerometer &getAccelerometer() const;

    const Gyroscope &getGyroscope() const;

public:
    Motion();
    Motion(Accelerometer a,Gyroscope g);
};


#endif //PRAKTIKUM_1_MOTION_H
