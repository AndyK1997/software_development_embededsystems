//
// Created by andyk on 24.04.2021.
//

#include "Motion.h"

Motion::Motion() {

}

Motion::Motion(Accelerometer a, Gyroscope g) {
    this->accelerometer=a;
    this->gyroscope=g;
}

const Accelerometer &Motion::getAccelerometer() const {
    return accelerometer;
}

const Gyroscope &Motion::getGyroscope() const {
    return gyroscope;
}
