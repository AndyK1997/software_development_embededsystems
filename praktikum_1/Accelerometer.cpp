//
// Created by andyk on 24.04.2021.
//

#include "Accelerometer.h"

Accelerometer::Accelerometer() {
    this->accelX=0;
    this->accelY=0;
    this->accelZ=0;
}

Accelerometer::Accelerometer(float x, float y, float z) {
    this->accelX=x;
    this->accelY=y;
    this->accelZ=z;
}

float Accelerometer::getAccelX() const {
    return accelX;
}

float Accelerometer::getAccelY() const {
    return accelY;
}

float Accelerometer::getAccelZ() const {
    return accelZ;
}
