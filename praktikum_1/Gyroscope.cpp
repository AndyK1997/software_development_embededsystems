//
// Created by andyk on 24.04.2021.
//

#include "Gyroscope.h"

Gyroscope::Gyroscope() {

}

Gyroscope::Gyroscope(float x, float y, float z) {
    this->gyroX=x;
    this->gyroY=y;
    this->gyroZ=z;
}

float Gyroscope::getGyroX() const {
    return gyroX;
}

float Gyroscope::getGyroY() const {
    return gyroY;
}

float Gyroscope::getGyroZ() const {
    return gyroZ;
}
