//
// Created by andyk on 24.04.2021.
//

#ifndef PRAKTIKUM_1_GYROSCOPE_H
#define PRAKTIKUM_1_GYROSCOPE_H


class Gyroscope {
private:
    float gyroX;
    float gyroY;
public:
    float getGyroX() const;

    float getGyroY() const;

    float getGyroZ() const;

private:
    float gyroZ;
public:
    Gyroscope();
    Gyroscope(float x, float y, float z);
};


#endif //PRAKTIKUM_1_GYROSCOPE_H
