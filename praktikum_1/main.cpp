#include <iostream>
#include "SensorConfiguration.h"
#include "SensorCommunication.h"
#include "SensorTag.h"
uint dataLaps = 10;

int main(int argc, char** argv) {

    SensorTag tag = SensorTag();
    tag.setAddr("24:71:89:E8:33:83");
    if(tag.initRead()==0){
        tag.writeMovementConfig();

        for( uint i = 0; i<dataLaps; i++){
            Motion m = tag.getMotion();

            std::cout << "gyro: " << m.getGyroscope().getGyroX() << ", " << m.getGyroscope().getGyroY() << ", " << m.getGyroscope().getGyroZ() << std::endl;
            std::cout << "acc: "  << m.getAccelerometer().getAccelX() << ", " << m.getAccelerometer().getAccelY() << ", " << m.getAccelerometer().getAccelZ() << std::endl;
        }
        tag.disconnect();
    }
    return 0;
}
