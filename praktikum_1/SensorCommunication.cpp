//
// Created by andyk on 24.04.2021.
//

#include <cstring>
#include <thread>
#include "SensorCommunication.h"
#define MAX_BUF 12
#define ARRAY_SIZE 3

Motion SensorCommunication::getMotion(SensorConfiguration conf) {
    char buffer[MAX_BUF];
    memset(buffer, 0, MAX_BUF);
    if (readMotion(conf, buffer) != 0) {
        std::cerr << "can't read motion" << std::endl;
    }
    return convertMotion(buffer);
}

Motion SensorCommunication::convertMotion(char* rawData) {
    int16_t *bit16 = reinterpret_cast<int16_t *>(rawData);
/* Umrechnung hier einfügen */
   float gyro[ARRAY_SIZE]{0};
   int index=0;
   for(unsigned int i=0; i<ARRAY_SIZE; i++){
       int val=0;

       if(bit16[index] <32768){
           val=bit16[index];
       }else{
           val=(bit16[index]-65536);
       }
       gyro[i]=((val*1.0)/(65536/500));
       index++;
   }
    //std::cout<<"gyro x: "<<gyro[0]<<" y: "<<gyro[1]<<" z: "<<gyro[2]<<std::endl;

    float acc[3]{0};
    for(unsigned int i=0;i<3;i++){

        int val=0;
        if(bit16[index]<32768){
            val=bit16[index];
        }else{
            val=(bit16[index]-65536);
        }
        acc[i]=((val*1.0)/(65536/4));
        index++;
    }
   // std::cout<<"acc x: "<<acc[0]<<" y: "<<acc[1]<<" z: "<<acc[2]<<std::endl;
    Gyroscope g(gyro[0],gyro[1], gyro[2]);
    Accelerometer a(acc[0], acc[1], acc[2]);
    Motion motion(a,g);

    return motion;
}

int SensorCommunication::readMotion(SensorConfiguration conf, char* buffer) {
    buffer[0] = 0x0d;
    buffer[1] = 0x01;
    buffer[2] = 0x6c;
    buffer[3] = 0xfc;
    buffer[4] = 0xd9;
    buffer[5] = 0xfc;
    buffer[6] = 0xf4;
    buffer[7] = 0xfd;
    buffer[8] = 0x85;
    buffer[9] = 0x00;
    buffer[10] = 0xd2;
    buffer[11] = 0x0f;

    /* Read movement data and display it */

    std::this_thread::sleep_for(std::chrono::seconds(1));
    return 0;
}

int SensorCommunication::writeConfig(SensorConfiguration conf){
    std::cout << "Write Config Platzhalter" <<std::endl;
    /* Activate the motion measurements */
    return 0;
}


