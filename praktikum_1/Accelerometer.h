//
// Created by andyk on 24.04.2021.
//

#ifndef PRAKTIKUM_1_ACCELEROMETER_H
#define PRAKTIKUM_1_ACCELEROMETER_H


class Accelerometer {
private:
    float accelX;
    float accelY;
public:
    float getAccelX() const;

    float getAccelY() const;

    float getAccelZ() const;

private:
    float accelZ;
public:
    Accelerometer();
    Accelerometer(float x, float y, float z);
};


#endif //PRAKTIKUM_1_ACCELEROMETER_H
