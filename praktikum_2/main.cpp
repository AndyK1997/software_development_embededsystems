#include <iostream>
#include "SensorConfiguration.h"
#include "SensorCommunication.h"
#include "SensorTag.h"
#include <pthread.h>
#include <thread>
#include <sys/time.h>

const int COUNT_OF_TRYS=10;

static  pthread_mutex_t  myMutex=PTHREAD_MUTEX_INITIALIZER;

Motion motion;
SensorTag tag = SensorTag();


void secureThreadArea(){
    pthread_mutex_lock(&myMutex);

    pthread_mutex_unlock(&myMutex);
}

void* readThread(void* arg){
    timeval tim;
    std::this_thread::sleep_for(std::chrono::nanoseconds(100)); //sync create Threads
    gettimeofday(&tim,NULL);
    std::cout<<"Start delay"<<tim.tv_usec<<std::endl;

    for(unsigned int x=0; x<COUNT_OF_TRYS; x++) {
        gettimeofday(&tim,NULL);
        int timeS=tim.tv_usec;

        Motion_t m = *((Motion *) arg);
        m = tag.getMotion();
        std::cout << "gyro: " << m.gyro.x << ", " << m.gyro.y << ", " << m.gyro.z << std::endl;
        std::cout << "acc: " << m.acc.x << ", " << m.acc.y << ", " << m.acc.z << std::endl;

        gettimeofday(&tim,NULL);
        int timeE=tim.tv_usec;
        std::cout<<"elapsed time read"<<timeE-timeS<<std::endl;
        std::this_thread::sleep_for(std::chrono::milliseconds (250));
    }
    return (void*)0;
}

void* resetThread(void* arg){
    timeval tim;
    gettimeofday(&tim,NULL);
    std::cout<<"Start delay"<<tim.tv_usec<<std::endl;
    for(unsigned int x=0; x<COUNT_OF_TRYS; x++) {
        gettimeofday(&tim,NULL);
        int timeS=tim.tv_usec;
        std::this_thread::sleep_for(std::chrono::milliseconds (1500));
        Motion_t m = *((Motion *) arg);
        m.gyro.x = 0;
        m.gyro.y = 0;
        m.gyro.z = 0;
        m.acc.x = 0;
        m.acc.y = 0;
        m.acc.z = 0;
        std::cout << "gyro: " << m.gyro.x << ", " << m.gyro.y << ", " << m.gyro.z << std::endl;
        std::cout << "acc: " << m.acc.x << ", " << m.acc.y << ", " << m.acc.z << std::endl;

        gettimeofday(&tim,NULL);
        int timeE=tim.tv_usec;
        std::cout<<"elapsed time reset"<<timeE-timeS<<std::endl;
    }
    return (void*)0;
}


int main(int argc, char** argv) {

    //SensorTag tag = SensorTag();
    tag.setAddr("24:71:89:E8:33:83");
    for(unsigned int x=0;  x<100; x++){
        std::printf("Hello");
    }

    for(unsigned int x=0;  x<100; x++){
        std::printf("Hello\n");
    }

    /*if(tag.initRead()==0){
        tag.writeMovementConfig();

        pthread_t read;
        pthread_t reset;

        pthread_create(&read,0,readThread,(void*)&motion);
        pthread_create(&reset,0,resetThread,(void*)&motion);

        pthread_join(read,0);
        pthread_join(reset,0);


        tag.disconnect();
    }*/
    return 0;
}
