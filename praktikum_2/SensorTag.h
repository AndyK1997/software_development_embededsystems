//
// Created by andyk on 24.04.2021.
//

#ifndef PRAKTIKUM_1_SENSORTAG_H
#define PRAKTIKUM_1_SENSORTAG_H
#include "SensorConfiguration.h"
#include "SensorCommunication.h"

class SensorTag : public SensorConfiguration, private SensorCommunication{
private:


public:
    SensorTag();
    int initRead();
    int writeMovementConfig();
    int disconnect();
    /*
        Reads motion sensor from SensorTag
        @return Return Motion_t with convertet values
    */
    Motion getMotion();
};


#endif //PRAKTIKUM_1_SENSORTAG_H
