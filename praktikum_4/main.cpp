#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/shm.h>
#include <wait.h>
#include "CCommQueue.h"
#include "SensorTag.h"
#include "CMessage.h"
#include <chrono>

using namespace std;

#define SHM_NAME        "/estSHM"
#define QUEUE_SIZE      16
#define NUM_MESSAGES    10

struct PackedData {
    Motion_t motion;
    UInt64 time;
};

typedef struct {
    int *test;
    CBinarySemaphore *cBinarySemaphore;
    CCommQueue *cCommQueue;
} Data_t;

UInt64 combine(UInt32 low, UInt32 high) {
    return (((UInt64) high << 32) | ((UInt64) low));
}

UInt32 high(UInt64 combined) {
    return combined >> 32;
}

UInt32 low(UInt64 combined) {
    UInt64 mask = std::numeric_limits<UInt32>::max();
    return mask & combined; // should I just do "return combined;" which gives same result?
}

Int8 *float_to_int8(Motion_t motion) {
    float *arrayF = new float[6];
    arrayF[0] = motion.gyro.x;
    arrayF[1] = motion.gyro.y;
    arrayF[2] = motion.gyro.z;
    arrayF[3] = motion.acc.x;
    arrayF[4] = motion.acc.y;
    arrayF[5] = motion.acc.z;
    Int8 *array = new Int8[sizeof(Motion)];
    array = (Int8 *) arrayF;
    return array;
}

Motion_t Int8_in_Motion(const Int8 *received_data) {
    float *motion_float = (float *) received_data;
    Motion_t motion;
    motion.gyro.x = motion_float[0];
    motion.gyro.y = motion_float[1];
    motion.gyro.z = motion_float[2];
    motion.acc.x = motion_float[3];
    motion.acc.y = motion_float[4];
    motion.acc.z = motion_float[5];
    return motion;
}

int main() {
    /**
     * Create shared Memory
     */
    cout << sizeof(Motion_t) << endl;
    int shm_id_sem = shmget(IPC_PRIVATE, sizeof(CBinarySemaphore), IPC_CREAT | 0644);
    int shm_id_int = shmget(IPC_PRIVATE, sizeof(int), IPC_CREAT | 0644);
    int shm_id_queue = shmget(IPC_PRIVATE, sizeof(CCommQueue), IPC_CREAT | 0644);
    if (shm_id_sem < 0) {
        cerr << "shmget() failed" << endl;
        exit(EXIT_FAILURE);
    }
    /**
     * Attach Shared Memory allen Prozessen
     */
    CBinarySemaphore *data = (CBinarySemaphore *) shmat(shm_id_sem, 0, 0);
    int *data_int = (int *) shmat(shm_id_int, 0, 0);
    CCommQueue *data_queue = (CCommQueue *) shmat(shm_id_queue, 0, 0);
    if (data == (CBinarySemaphore *) -1) {
        cerr << "shmat() failed" << endl;
        exit(EXIT_FAILURE);
    }
    /**
     * Markiert den Shared Memory zur löschung wenn kein referenz mehr besteht
     */
    shmctl(shm_id_sem, IPC_RMID, 0);
    shmctl(shm_id_int, IPC_RMID, 0);
    shmctl(shm_id_queue, IPC_RMID, 0);

    /**
     * Alloc pointer mit placement new
     */
    data = new(data) CBinarySemaphore();
    data_int = new(data_int) int(0);
    data_queue = new(data_queue) CCommQueue(QUEUE_SIZE, (*data));

    // (*data).give();

    pid_t pid = fork();

    if (pid < 0) {
        cerr << "frok() failed" << endl;
        exit(EXIT_FAILURE);
    } else if (pid == 0) {
        //child
        Int64 timeDiffTotal = 0;
        int cntR = 0;

        while (cntR < NUM_MESSAGES) {
            CMessage cMessage;
            PackedData packedData;
            //(*data).takeWithTimeOut(-1);
            if ((*data_queue).getMessage(cMessage)) {
                UInt64 timeNow = std::chrono::duration_cast<std::chrono::microseconds>(
                        std::chrono::system_clock::now().time_since_epoch()).count();
                UInt64 timeRecv = combine(cMessage.getParam2(), cMessage.getParam1());
                UInt64 timeDiff = timeNow - timeRecv;
                timeDiffTotal += timeDiff;
                packedData.motion = Int8_in_Motion(cMessage.getParam4());
                cout << "Child, recived msgID: " << cMessage.getSenderID() << ", QueueSize: "
                     << (*data_queue).getNumOfMessages() << ", Data: gyro: " << packedData.motion.gyro.x << ","
                     << packedData.motion.gyro.y << "," << packedData.motion.gyro.z <<", acc: "
                     << packedData.motion.acc.x << "," << packedData.motion.acc.y << "," << packedData.motion.acc.z
                     << ", transfer: " << timeRecv << ", " << timeNow << ", " << timeDiff << endl << flush;
                cntR++;
                (*data_int)++;
            }
            //(*data).give();
        }
        cout << "TransferDiff Total: " << timeDiffTotal / cntR << " microseconds" << endl << flush;
        cout << "exit child" << endl << flush;
    } else if (pid > 0) {
        //parent
        int cntW = 0;
        static int send_id = 0;
        SensorTag sensorTag;

        while (cntW < NUM_MESSAGES) {
            CMessage cMessage;
            cMessage.setSenderID(send_id);
            PackedData packedData;
            packedData.motion = sensorTag.getMotion();
            UInt64 timeNow = std::chrono::duration_cast<std::chrono::microseconds>(
                    std::chrono::system_clock::now().time_since_epoch()).count();;
            packedData.time = timeNow;
            //(*data).takeWithTimeOut(-1);
            cMessage.setParam1(high(packedData.time));
            cMessage.setParam2(low(packedData.time));
            cMessage.setParam4(float_to_int8(packedData.motion), sizeof(packedData.motion));
            if ((*data_queue).add(cMessage)) {
                cout << "Parent, send msgID: " << cMessage.getSenderID() << ", QueueSize: "
                     << (*data_queue).getNumOfMessages() << ", transfer: " << timeNow << endl << flush;
                cntW++;
                send_id++;
            }
            //(*data).give();
            //sleep(1);
        }
        waitpid(pid, NULL, 0);
        cout << "Read Msg: " << (*data_int) << " of " << NUM_MESSAGES << endl << flush;
        cout << "Queue Size: " << (*data_queue).getNumOfMessages() << endl << flush;
    }
    /**
     * Detach
     */
    shmdt(data);
    shmdt(data_int);
    shmdt(data_queue);
    return 0;
}
//Queue Size 01 ->16 microseconds
//Queue Size 04 ->17 microseconds
//Queue Size 16 ->19 microseconds