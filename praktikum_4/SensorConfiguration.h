//
// Created by andyk on 24.04.2021.
//

#ifndef PRAKTIKUM_1_SENSORCONFIGURATION_H
#define PRAKTIKUM_1_SENSORCONFIGURATION_H
#include <string>


class SensorConfiguration {
private:
    /*
			MAC address of the SensorTag
		*/
    std::string addr;

public:
    SensorConfiguration();
    /*
			Return the MAC addres of the SensorTag
			@return	MAC address as std::string
		*/
    const std::string &getAddr() const;
    /*
			Sets the MAC address of the SensorTag
			@param[in] addr	MAC addes
		*/
    void setAddr(const std::string &addr);
    /*
			Connect to a Sensortag with the given adress
			and verify that it has a movement service and
			the described characteristics
			@return 0 for sucess, -1 for device not found,
			-2 for Service not found and -3 for wrong
			characteristics
		*/
    virtual int initializeSensortag();

    virtual int disconnectBLEDevice();
};


#endif //PRAKTIKUM_1_SENSORCONFIGURATION_H
