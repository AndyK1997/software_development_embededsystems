// File     sync_proc.h
// Version  1.0
// Author   Jens-Peter Akelbein
// Comment  Softwareentwicklung fuer Embedded Systeme - Exercise 3

#include <iostream>
#include "CNamedSemaphore.h"
#include <stdlib.h>
#include <unistd.h>
#include "Motion.h"
#include "SensorTag.h"

using namespace std;

Motion motion;
SensorTag tag = SensorTag();
int loopCounter=0;

// valid states for our two processes, we use the impicit ordering of values
// by an enum starting with the value 1
enum EProc_State {
    STATE_ACTIVE_CHILD = 1,
    STATE_ACTIVE_PARENT,
    STATE_TERMINATE
};

#define NUMBER_OF_LOOPS     10

const char sem_name1[] = "/semaphore";
const char sem_name2[] = "/state";
CNamedSemaphore semaphore(sem_name1, 1);
CNamedSemaphore state(sem_name2, STATE_ACTIVE_CHILD);


void readMotion(){
    motion = tag.getMotion();
    std::cout << "gyro: " << motion.gyro.x << ", " << motion.gyro.y << ", " << motion.gyro.z << std::endl;
    std::cout << "acc: " << motion.acc.x << ", " << motion.acc.y << ", " << motion.acc.z << std::endl;
}

void resetMotion(){
    motion.gyro.x = 0; motion.gyro.y = 0; motion.gyro.z = 0; motion.acc.x = 0; motion.acc.y = 0; motion.acc.z = 0;
    std::cout << "gyro: " << motion.gyro.x << ", " << motion.gyro.y << ", " << motion.gyro.z << std::endl;
    std::cout << "acc: " << motion.acc.x << ", " << motion.acc.y << ", " << motion.acc.z << std::endl;
}
// function being executed as parent or as child process to perform ping pong
// between both processes
void pingpong(bool parent) {
    while(loopCounter<NUMBER_OF_LOOPS){
        /*if (parent) {
            semaphore.decrement();
            //std::cout << "pong" << semaphore.value() << endl << flush;
            readMotion();
            if (state.value() < 1) {
                state.increment();
            }
        } else {
            state.decrement();
           // std::cout << "ping" << semaphore.value() << endl << flush;
            resetMotion();
            if (semaphore.value() < 1) {
                semaphore.increment();
            }
        }*/
        if(parent){
            if(state.value()==STATE_ACTIVE_PARENT){
                semaphore.decrement(); //lock
                std::cout << "pong" << state.value() << endl << flush;
                readMotion();
                loopCounter++;
                semaphore.increment(); //unlock;
                state.decrement();
            }
        }else{
            if(state.value()==STATE_ACTIVE_CHILD){
                semaphore.decrement(); //lock
                std::cout << "ping" << state.value() << endl << flush;
                resetMotion();
                loopCounter++;
                semaphore.increment(); //unlock
                state.increment();
            }
        }
    }
    state.increment();
}

// main function, here we are just forking into two processes both calling
// pingpong() and indicating with a boolean on who is who
int main() {
    tag.setAddr("24:71:89:E8:33:83");
    if(tag.initRead()==0) {
        tag.writeMovementConfig();
        // now we fork...
        pid_t pid = fork();

        if (pid == 0) {
            //Child Process;
            cout << "Hello from Child process pid: " << getpid() << endl;
            pingpong(false);
        } else {
            //Parent Process;
            cout << "Hello from Parent process pid: " << getpid() << endl;
            pingpong(true);
        }

        if (state.value() == STATE_TERMINATE) {
            cout << "exit Program" << endl;
        }
        tag.disconnect();
    }
    return 0;
}
