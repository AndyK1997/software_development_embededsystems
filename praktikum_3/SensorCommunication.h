//
// Created by andyk on 24.04.2021.
//

#ifndef PRAKTIKUM_1_SENSORCOMMUNICATION_H
#define PRAKTIKUM_1_SENSORCOMMUNICATION_H
#include <cstdlib>
#include <cstddef>
#include <iostream>
#include <bitset>
#include "Motion.h"
#include "SensorConfiguration.h"

class SensorCommunication {
private:
    /*
			Reads the Motion from the SensorTag specified by the given configuration.
			@param[in] conf Configuration of the SensorTag
			@param[out] buffer A 12 byte array that will be filled by the function with
			the raw data of the motion sensor.
			@return 0 for success, -1 for failure
		*/
    virtual int readMotion(SensorConfiguration conf, char* buffer);

    /*
        Converts the raw data of the motion sensor and return the converted values.
        @param[in] rawData	A 12 byte array that contains the raw data of the motion sensor.
        @return Motion_t filled with the converted motion values
    */
    virtual Motion convertMotion(char* rawData);
    std::byte *rawDataStream;
public:
    /*
			Reads the motion sensor from the SensorTag specified by the given SensorTag
			confoguration, converts the raw data and returns it as Motion_t.
			@param[in] conf Configuration of the SensorTag
			@return Motion values
		*/
    Motion getMotion(SensorConfiguration conf);
    int writeConfig(SensorConfiguration conf);
};


#endif //PRAKTIKUM_1_SENSORCOMMUNICATION_H
