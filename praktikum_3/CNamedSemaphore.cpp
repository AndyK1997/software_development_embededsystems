// File     CNamedSemaphore.cpp
// Version  1.0
// Author   Jens-Peter Akelbein
// Comment  Softwareentwicklung fuer Embedded Systeme - Exercise 3

#include <iostream>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include "CNamedSemaphore.h"

using namespace std;

// construct a new named semaphore
// @name - name of the semaphore being created in /run/shm
// @value - initial value of the semaphore
CNamedSemaphore::CNamedSemaphore(const char *name, int value) {
    this->remember_my_name=name;
    sem_unlink(this->remember_my_name);
    semaphore= sem_open(name,O_CREAT | O_EXCL,0644,value);
}

// deconstruct the semaphore
// question:
// How do we delete the semaphore by the last process only?
CNamedSemaphore::~CNamedSemaphore() {
    sem_destroy(semaphore);
    sem_unlink(this->remember_my_name);
    sem_close(semaphore);

}


void CNamedSemaphore::increment(void) {
    sem_post(semaphore);
}


void CNamedSemaphore::decrement(void) {
    sem_wait(semaphore);
}


int CNamedSemaphore::value(void) {
    int currentSemValue=0;
    sem_getvalue(semaphore,&currentSemValue);
    return currentSemValue;
}


// helper function to display errors and terminate our
// process as a very simple error handling mechanism
void CNamedSemaphore::exitproc(const char *text, int err) {

    cout << text;
    switch (err) {
        case EACCES:
            cerr << "EACCES";
            break;
        case EEXIST:
            cerr << "EEXIST";
            break;
        case EINVAL:
            cerr << "EINVAL";
            break;
        case EMFILE:
            cerr << "EMFILE";
            break;
        case ENAMETOOLONG:
            cerr << "TOOLNG";
            break;
        case ENOENT:
            cerr << "ENOENT";
            break;
        case ENOMEM:
            cerr << "ENOMEM";
            break;
        case EOVERFLOW:
            cerr << "EOVERFLOW";
            break;
        default:
            cerr << "UNKNWN";
    }
    cout << endl;
    exit(1);
}
