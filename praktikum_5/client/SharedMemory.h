//
// Created by AndyK on 23.06.2021.
//
#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/shm.h>
#include <wait.h>
#include "CCommQueue.h"
#include "SensorTag.h"
#include "CMessage.h"
#include <chrono>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#ifndef CLIENT_SHAREDMEMORY_H
#define CLIENT_SHAREDMEMORY_H

#endif //CLIENT_SHAREDMEMORY_H

/**
 * Create shared Memory
 */
void createSharedMemory(int &id_sem, int &id_int, int &id_queue) {
    id_sem = shmget(IPC_PRIVATE, sizeof(CBinarySemaphore), IPC_CREAT | 0644);
    id_int = shmget(IPC_PRIVATE, sizeof(int), IPC_CREAT | 0644);
    id_queue = shmget(IPC_PRIVATE, sizeof(CCommQueue), IPC_CREAT | 0644);
    if (id_sem < 0 || id_int < 0 || id_queue < 0) {
        std::cerr << "shmget() failed" << std::endl;
        exit(EXIT_FAILURE);
    }
}

/**
* Attach Shared Memory allen Prozessen
*/
void attachSharedMemory(CBinarySemaphore *data, int *data_int, CCommQueue *data_queue, int id_sem, int id_int,
                        int id_queue) {
    data = (CBinarySemaphore *) shmat(id_sem, 0, 0);
    data_int = (int *) shmat(id_int, 0, 0);
    data_queue = (CCommQueue *) shmat(id_queue, 0, 0);
    if (data == (CBinarySemaphore *) -1 || data_int == (int *) -1 || data_queue == (CCommQueue *) -1) {
        std::cerr << "shmat() failed" << std::endl;
        exit(EXIT_FAILURE);
    }
}

void detachSharedMemory(CBinarySemaphore *data, int *data_int, CCommQueue *data_queue){
    shmdt(data);
    shmdt(data_int);
    shmdt(data_queue);
}

/**
 * Markiert den Shared Memory zur löschung wenn kein referenz mehr besteht
 */
void markForRemoveSharedMemory(int &id_sem, int &id_int, int &id_queue) {
    shmctl(id_sem, IPC_RMID, 0);
    shmctl(id_int, IPC_RMID, 0);
    shmctl(id_queue, IPC_RMID, 0);
}

