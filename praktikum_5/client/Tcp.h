//
// Created by AndyK on 23.06.2021.
//

#ifndef CLIENT_TCP_H
#define CLIENT_TCP_H

#endif //CLIENT_TCP_H
#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/shm.h>
#include <wait.h>
#include "CCommQueue.h"
#include "SensorTag.h"
#include "CMessage.h"
#include <chrono>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>

struct PackedData {
    Motion_t motion;
    UInt64 time;
};

void createSocket(int &sock, sockaddr_in &hint, std::string clientIp, std::string clientPort) {
    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock == -1) {
        std::cerr << "socket create failed" << std::endl;
        exit(EXIT_FAILURE);
    }
    hint.sin_family = AF_INET;
    hint.sin_port = htons(atoi(clientPort.c_str()));
    inet_pton(AF_INET, clientIp.c_str(), &hint.sin_addr);
}

void getPackedDataFromServer(PackedData &packedData, sockaddr_in &hint, int sock) {
    char buf[4096];
    while (true) {
        int connectRes = connect(sock, (sockaddr *) &hint, sizeof(hint));
        if (connectRes == -1) {
            std::cerr << "Connection failed server offline" << std::endl;
            sleep(1);
            continue;
        }
        std::string msg = "Req SensorTag Data";
        int sendRes = send(sock, msg.c_str(), msg.size() + 1, 0);
        if (sendRes == -1) {
            std::cout << "Could not send to server!"<<std::endl;
            continue;
        } else {


            memset(buf, 0, 4096);
            int bytesReceived = recv(sock, buf, 4096, 0);
            if (bytesReceived == -1) {
                std::cout << "receiving error from server"<<std::endl;
                break;
            } else {
                //		Display response
                std::cout << "SERVER> " << std::string(buf, bytesReceived) << "\r\n";
                break;
            }
        }
    }
    packedData = *(PackedData *) buf;
    close(sock);
}