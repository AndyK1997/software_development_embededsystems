#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/shm.h>
#include <wait.h>
#include "CCommQueue.h"
#include "SensorTag.h"
#include "CMessage.h"
#include <chrono>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include "SharedMemory.h"
#include "Tcp.h"
using namespace std;

#define SHM_NAME        "/estSHM"
#define QUEUE_SIZE      10
#define NUM_MESSAGES    100


/**
 * Alloc pointer mit placement new
 */
void allocPlacementNew(CBinarySemaphore *data, int *data_int, CCommQueue *data_queue) {
    data = new(data) CBinarySemaphore();
    data_int = new(data_int) int(0);
    data_queue = new(data_queue) CCommQueue(QUEUE_SIZE, (*data));
}




int main(int argc, char *argv[]) {

    if (argc < 3) {
        cerr << "missing args" << endl;
        exit(EXIT_FAILURE);
    }
    cout << "IP: " << argv[1] << ", Port: " << argv[2] << endl;
    std::string clientIp = argv[1], clientPort = argv[2];

    int shm_id_sem, shm_id_int, shm_id_queue;
    createSharedMemory(shm_id_sem, shm_id_int, shm_id_queue);

    /**
 * Attach Shared Memory allen Prozessen
 */
    CBinarySemaphore *data = (CBinarySemaphore *) shmat(shm_id_sem, 0, 0);
    int *data_int = (int *) shmat(shm_id_int, 0, 0);
    CCommQueue *data_queue = (CCommQueue *) shmat(shm_id_queue, 0, 0);
    if (data == (CBinarySemaphore *) -1 || data_int == (int *) -1 || data_queue == (CCommQueue *) -1) {
        cerr << "shmat() failed" << endl;
        exit(EXIT_FAILURE);
    }

    markForRemoveSharedMemory(shm_id_sem, shm_id_int, shm_id_queue);
    allocPlacementNew(data, data_int, data_queue);

    pid_t pid = fork();

    if (pid < 0) {
        cerr << "frok() failed" << endl;
        exit(EXIT_FAILURE);
    } else if (pid == 0) {
        //child
        Int64 timeDiffTotal = 0;
        int cntR = 0;
        while (true) {
            CMessage cMessage;
            PackedData packedData;
            if ((*data_queue).getMessage(cMessage)) {
                UInt64 timeNow = std::chrono::duration_cast<std::chrono::microseconds>(
                        std::chrono::system_clock::now().time_since_epoch()).count();
                memcpy(&packedData,cMessage.getParam4(),sizeof(PackedData));
                UInt64 timeRecv=packedData.time;
                UInt64 timeDiff = timeNow - timeRecv;
                timeDiffTotal += timeDiff;
                cout << "Emb_Client Child, recived msgID: " << cMessage.getSenderID() << ", QueueSize: "
                     << (*data_queue).getNumOfMessages() << ", Data: gyro: " << packedData.motion.gyro.x << ","
                     << packedData.motion.gyro.y << "," << packedData.motion.gyro.z << ", acc: "
                     << packedData.motion.acc.x << "," << packedData.motion.acc.y << "," << packedData.motion.acc.z
                     << ", transfer: " << timeRecv << ", " << timeNow << ", " << timeDiff << endl << flush;

                cntR++;
                (*data_int)++;
            }

        }
        cout << "TransferDiff Total: " << timeDiffTotal / cntR << " microseconds" << endl << flush;
        cout << "exit child" << endl << flush;
    } else if (pid > 0) {
        //parent
        int cntW = 0;
        static int send_id = 0;
        int sock;
        sockaddr_in hint;

        while (true) {
            CMessage cMessage;
            cMessage.setSenderID(send_id);
            PackedData packedData;
            createSocket(sock, hint, clientIp, clientPort);
            getPackedDataFromServer(packedData, hint, sock);

            Int8 buffer[sizeof(PackedData)];
            memcpy(&buffer,&packedData,sizeof(PackedData));
            cMessage.setParam4(buffer,sizeof(PackedData));

            if ((*data_queue).add(cMessage)) {
                cout << "Emb_Client Parent, send msgID: " << cMessage.getSenderID() << ", QueueSize: "
                     << (*data_queue).getNumOfMessages() << ", transfer: " << packedData.time << endl << flush;
                cntW++;
                send_id++;
            }

        }


        waitpid(pid, NULL, 0);
        cout << "Read Msg: " << (*data_int) << " of " << NUM_MESSAGES << endl << flush;
        cout << "Queue Size: " << (*data_queue).getNumOfMessages() << endl << flush;
    }
    /**
     * Detach
     */
    detachSharedMemory(data,data_int,data_queue);
    return 0;
}
//Queue Size 01 ->16 microseconds
//Queue Size 04 ->17 microseconds
//Queue Size 16 ->19 microseconds