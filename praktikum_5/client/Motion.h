 //
// Created by andyk on 24.04.2021.
//

#ifndef PRAKTIKUM_1_MOTION_H
#define PRAKTIKUM_1_MOTION_H

struct Accelerometer {
    float x;
    float y;
    float z;
};
typedef struct Accelerometer Accelerometer_t;

struct Gyroscope {
    float x;
    float y;
    float z;
};
typedef struct Gyroscope Gyroscope_t;

struct Motion {
    struct Gyroscope gyro;
    struct Accelerometer acc;
};
typedef struct Motion Motion_t;



#endif //PRAKTIKUM_1_MOTION_H
