FROM andyk1997/uni:latest
ENV IP "$DEST_SERVER_IP"
ENV PORT "$DEST_SERVER_PORT"
COPY ./client /usr/src/
WORKDIR /usr/src/
RUN cmake .
RUN make
RUN ls -la
RUN chmod 755 client
CMD ./client $DEST_SERVER_IP $DEST_SERVER_PORT