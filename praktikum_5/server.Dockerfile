FROM andyk1997/uni:latest
ENV IP "$SERVER_IP"
ENV PORT "$SERVER_PORT"
COPY ./server /usr/src/
WORKDIR /usr/src/ 
RUN cmake .
RUN make
RUN ls -la
RUN chmod 755 server
CMD ./server $SERVER_IP $SERVER_PORT