//
// Created by AndyK on 23.06.2021.
//
#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/shm.h>
#include <wait.h>
#include "CCommQueue.h"
#include "SensorTag.h"
#include "CMessage.h"
#include <chrono>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#ifndef SERVER_TCP_H
#define SERVER_TCP_H

#endif //SERVER_TCP_H

struct PackedData {
    Motion_t motion;
    UInt64 time;
};

void createSocket(int &listening, sockaddr_in &hint, std::string serverPort){
    listening = socket(AF_INET, SOCK_STREAM, 0);
    hint.sin_family = AF_INET;
    hint.sin_port = htons(atoi(serverPort.c_str()));
    inet_pton(AF_INET, "0.0.0.0", &hint.sin_addr);
    bind(listening, (sockaddr *) &hint, sizeof(hint));
    listen(listening, SOMAXCONN);
}

void waitAndHandleEstablishedConnection(int &listening, std::byte *packedDataInByte){
    sockaddr_in client;
    socklen_t clientSize = sizeof(client);
    int clientSocket = accept(listening, (sockaddr *) &client, &clientSize);
    char host[NI_MAXHOST];
    char service[NI_MAXSERV];
    memset(host, 0, NI_MAXHOST);
    memset(service, 0, NI_MAXSERV);

    //DNS IP ADDRESS
    if (getnameinfo((sockaddr *) &client, sizeof(client), host, NI_MAXHOST, service, NI_MAXSERV, 0) == 0) {
        std::cout << host << " connected on port " << service << " Socket id: " << clientSocket
                  << std::endl;
    }
        //IP ADDRESS
    else {
        inet_ntop(AF_INET, &client.sin_addr, host, NI_MAXHOST);
        std::cout << host << " connected on port " << ntohs(client.sin_port) << " Socket id: "
                  << clientSocket << std::endl;
    }

    char buf[4096];
    int newSocket = clientSocket;
    std::cout<<sizeof(*packedDataInByte)<<std::endl;
    send(newSocket, packedDataInByte, sizeof(PackedData), 0);

    // While loop: accept and echo message back to client
    while (true) //listen to current host
    {
        memset(buf, 0, 4096);
        // Wait for client to send data
        int bytesReceived = recv(newSocket, buf, 4096, 0);
        //INTERRUPT CONNECTION OR ABNORMALLY CLIENT DISCONNECT
        if (bytesReceived == -1) {
            std::cerr << "Error in recv(). Quitting" << std::endl;
            break;
        }
            //CLIENT DISCONNECTED NORMALLY
        else if (bytesReceived == 0) {
            std::cout << "Client disconnected Socket id: " << newSocket << std::endl;
            break;
        }

        //send(newSocket, buf, bytesReceived + 1, 0);
        //send(newSocket, packedDataInByte,sizeof(packedDataInByte)+1, 0);
    }

    close(newSocket);
}
